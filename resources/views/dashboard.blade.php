<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'URL-Shorter') }}</title>

    <!-- Scripts -->

    {!! Html::style('assets/css/bootstrap.min.css') !!}
    <!-- App css -->
    {!! Html::style('assets/css/icons.min.css') !!}
    {!! Html::style('assets/libs/toastr/toastr.min.css') !!}
    {!! Html::style('assets/css/style.css') !!}

    </head>
    <body>


        <nav class="navbar navbar-dark bg-primary navbar-expand-md shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Url-Shorter') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        @if(Auth::user()->type == 'is_admin')
                            <li class="nav-item">
                                <a href="{{ url('/admin') }}" class="nav-link">Admin Panel</a>
                            </li>
                            @endif
                            <li class="nav-item dropdown active">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name. ((Auth::user()->type != 'is_admin')?'(Guest)':'') }}

                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        <div class="container">

            <h3 class="text-center">Short Your links</h3>


            <div class="container p-4" style="background-color: #0b1736;">

                <div class="card">
                    <div class="card-body">

                        {!! Form::open([
                            'route' => 'url_shorter',
                            'class' => 'form_validated form-horizontal form-label-left
                            col-md-12',
                            'role' => 'form',
                            ]) !!}
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-10 float-left">
                                        <input type="text" name="url" class="form-control" placeholder="Shorten your link">
                                    </div>
                                    <div class="col-md-2 float-left">
                                        <button class="btn btn-primary">Shorten</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                @if (Session::get('data'))



                @php
                    $result = Session::get('data');
                @endphp
                <div class="card">
                    <div class="card-body pb-4">
                        <h2 class="card-title bg-dark text-white p-2">
                            it's valid for only 7 days <br>
                            if you want to customize, brand, and track your links?
                            <a href="{{ route('register') }}">Register</a>

                        </h2>
                        <h2 class="card-title border-bottom">
                            Your Shorted URL
                        </h2>
                        <div class="row">
                            <div class="col-9 float-left text-danger">
                                <div>
                                    <label data-toggle="tooltip" data-placement="bottom" title="{{  $result->url }}">
                                        {{ URL('urlRedirect?url=' . $result->domain . '-' . $result->short)  }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-3 float-left">
                                <a href="#" class="btn btn-primary waves-effect btn-sm waves-light copyToClipboard"
                                txt="{{ URL('urlRedirect?url=' . $result->domain . '-' . $result->short) }}" role="button">
                                <i class="fa fa-copy"></i> Copy</a>

                                <a href="{{ URL('urlRedirect?url=' . $result->domain . '-' . $result->short)  }}"
                                    target="url" class="btn btn-primary waves-effect btn-sm waves-light"
                                txt="aaa" role="button">
                                <i class="fa fa-link"></i> Open</a>
                            </div>
                        </div>

                    </div>
                </div>
                @endif

            </div>


            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-center">Frequently asked questions</h3>
                    <div class="row">

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            What is a URL Shortener?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>A URL shortener, also known as a link shortener, seems like a simple tool, but it is a service that can have a dramatic impact on your marketing efforts.</p>
                                        <p>Link shorteners work by transforming any long URL into a shorter, more readable link. When a user clicks the shortened version, they’re automatically forwarded to the destination URL.</p>
                                        <p>
                                            Think of a short URL as a more descriptive and memorable nickname for your long webpage address. You can, for example, use a short URL like bit.ly/CelebrateBitly so people will have a good idea about where your link
                                            will lead before they click it.
                                        </p>
                                        <p>If you’re contributing content to the online world, you <em>need</em> a URL shortener.</p>
                                        <p>Make your URLs stand out with our easy to use free link shortener above.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Benefits of a Short URL
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            How many people can even remember a long web address, especially if it has tons of characters and symbols? A short URL can make your link more memorable. Not only does it allow people to easily recall and share your
                                            link with others, it can also dramatically improve traffic to your content.
                                        </p>
                                        <p>On a more practical side, a short URL is also easier to incorporate into your collateral – whether you’re looking to engage with your customers offline or online.</p>
                                        <p>Bitly is the best URL shortener for everyone, from influencers to small brands to large enterprises, who are looking for a simple way to create, track and manage their links.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    What is a Custom URL Shortener?
                                </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>A custom URL shortener, sometimes referred to as a branded URL shortener, lets you brand your links.</p>
                                    <p>For example, instead of bit.ly/xyz, you could use a custom short URL like yourbrnd.co/xyz.</p>
                                    <p>
                                        There are several benefits of branding your short links. Branded links build trust between your audience and your business, drive more clicks, give your audience a preview of where they are being taken and increase
                                        brand awareness.
                                    </p>
                                    <p>A link shortening service that includes custom short URLs is vital to improving audience engagement with your communications. A short URL is good, but a custom URL works every time.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    </body>

    <script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    {!! Html::script('assets/libs/toastr/toastr.min.js') !!}

    {!! Html::script('assets/js/script.js') !!}
</html>
