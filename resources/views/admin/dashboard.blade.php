@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="card-box col-md-12">
            <h4 class="header-title">URL Shortener Statistics</h4>

            <div class="row">

                <div id="barchart_material" style="width: 100%; height: 500px;"></div>
            </div>
         
            </div>
        </div>
    </div>

    @section('ext_lib')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @endsection

    @section('custom_script')

    <script type="text/javascript">
         
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);
   
        function drawChart() {
          var data = google.visualization.arrayToDataTable([
              ['Date', 'Shorten'],
   
              @php
                foreach($result as $item) {
                    echo "['".$item->date."', ".$item->count."],";
                }
              @endphp
          ]);
   
          var options = {
            chart: {
              title: 'Bar Graph By Number of shorten',
            },
            bars: 'vertical'
          };
          var chart = new google.charts.Bar(document.getElementById('barchart_material'));
          chart.draw(data, google.charts.Bar.convertOptions(options));
        }
      </script>

      @endsection

@stop
