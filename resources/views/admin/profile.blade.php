@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="card-box col-md-3">

        <div class="member-card">
            <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                <img src="{{ URL('assets/avatar.jpg') }}" class="rounded-circle img-thumbnail"
                    alt="profile-image">
                <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
            </div>

            <div class="">
                <h5 class="font-18 mb-1">{{Auth::user()->name}}</h5>
                <p class="text-muted mb-2">@web-developer</p>
            </div>

            <button type="button" class="btn btn-success btn-sm width-sm waves-effect mt-2 waves-light">Follow</button>
            <button type="button" class="btn btn-danger btn-sm width-sm waves-effect mt-2 waves-light">Message</button>

            <p class="sub-header mt-3">
                Hi I'm Johnathn Deo,has been the industry's standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type.
            </p>

            <hr />

            <div class="text-left">
                <p class="text-muted font-13"><strong>Full Name :</strong> <span class="ml-4">{{Auth::user()->name}}</span></p>

                <p class="text-muted font-13"><strong>Mobile :</strong><span class="ml-4">01674416623</span></p>

                <p class="text-muted font-13"><strong>Email :</strong> <span class="ml-4">{{Auth::user()->email}}</span>
                </p>

                <p class="text-muted font-13"><strong>Location :</strong> <span class="ml-4">Dhaka</span></p>
            </div>

            <ul class="social-links list-inline mt-4">
                <li class="list-inline-item">
                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#"
                        data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="list-inline-item">
                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#"
                        data-original-title="Twitter"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="list-inline-item">
                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#"
                        data-original-title="Skype"><i class="fab fa-skype"></i></a>
                </li>
            </ul>

        </div>
    </div>
    <div class="card-box col-md-4">
        <h4 class="header-title">Update My Profile</h4>

        <div class="row">
            <form action="{{ URL('admin/profile/update')}}" method="post" class="form-horizontal">
                @csrf

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    <span class="required text-danger">*</span>

                    <div>
                        {!! Form::text('name', Auth::user()->name, ['class' => 'form-control placeholder',
                        "parsley-trigger"=>"change",'required'=>'required']) !!}
                        <span class="help-block text-danger">
                            {!! $errors -> first('name') !!}
                        </span>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                    {!! Form::label('old_password', 'Old Password', ['class' => 'control-label']) !!}
                    <span class="required text-danger">*</span>

                    <div>
                        {!! Form::text('old_password', null, ['class' => 'form-control placeholder',
                        "parsley-trigger"=>"change",'required'=>'required']) !!}
                        <span class="help-block text-danger">
                            {!! $errors -> first('old_password') !!}
                        </span>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                    {!! Form::label('new_password', 'New Password', ['class' => 'control-label']) !!}
                    <span class="required text-danger">*</span>

                    <div>
                        {!! Form::text('new_password', null, ['class' => 'form-control placeholder',
                        "parsley-trigger"=>"change",'required'=>'required']) !!}
                        <span class="help-block text-danger">
                            {!! $errors -> first('new_password') !!}
                        </span>
                    </div>
                </div>


                <div class="form-group">
                    <div class=" pull-right">
                        <button class="btn btn-primary" type="button">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </form>

        </div>
    </div>


</div>

@stop