@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="card-box col-md-12">
            <h4 class="header-title">List of Shorter</h4>

                <table id="datatable_by_ajax" method="admin/shorterAjax"
                 columns="action,original_url,short_url,short,domain,type,expired"
                    class="table table-striped table-hover col-md-12">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Original URL</th>
                            <th>Short URL</th>
                            <th>Short</th>
                            <th>Alias</th>
                            <th>Type</th>
                            <th>Expired</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

        </div>
    </div>

@stop
