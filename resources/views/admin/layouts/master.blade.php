<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
        {{ (isset($title) && !empty($title)) ? $title : ucfirst(str_replace('_', ' ',request()->segment(2))). ' '.
        (request()->segment(4) ? request()->segment(4) : '') }}
    </title>


    {!! Html::style('assets/css/bootstrap.min.css') !!}
    <!-- App css -->
    {!! Html::style('assets/css/icons.min.css') !!}
    {!! Html::style('assets/libs/toastr/toastr.min.css') !!}

    @if(isset($lib['select2']))
    {!! Html::style('assets/libs/select2/select2.min.css') !!}
    @endif
    @if(isset($lib['datatable']))
    {!! Html::style('assets/libs/datatables/dataTables.bootstrap4.min.css') !!}
    {!! Html::style('assets/libs/datatables/responsive.bootstrap4.min.css') !!}
    @endif
    @if(isset($lib['bootstrap-toggle']))
    {!! Html::style('assets/libs/bootstrap-toggle/bootstrap-toggle.min.css') !!}
    @endif
    @if(isset($lib['datetimepicker']))
    {!! Html::style('assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}
    @endif

    {!! Html::style('assets/css/app.min.css') !!}
    {!! Html::style('assets/css/style.css') !!}


    <script>
        var urlD = "{{ url('/') }}";
        var $token = '{{ csrf_token() }}';

    </script>
</head>


<body>

    <!-- Begin page -->
    <div id="wrapper">


        @include('admin.layouts.navbar')

        @include('admin.layouts.leftbar')


        <!--End Menu-->

        <!--Start Content-->
        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    @include('flash::message')
                    @yield('content')
                </div>
            </div>
        </div>
        <!--End Content-->

        <!--  Start Footer -->
        @include('admin.layouts.footer')

        <!--  End Footer -->
    </div>

    {{--Scripts--}}


    {!! Html::script('assets/js/vendor.min.js') !!}
    {!! Html::script('assets/libs/parsleyjs/parsley.min.js') !!}
    {!! Html::script('assets/libs/toastr/toastr.min.js') !!}


    @if(isset($lib['select2']))
    {!! Html::script('assets/libs/select2/select2.min.js') !!}
    @endif

    @if(isset($lib['datatable']))
    {!! Html::script('assets/libs/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/libs/datatables/dataTables.bootstrap4.min.js') !!}
    @endif


    @if(isset($lib['bootstrap-toggle']))
    {!! Html::script('assets/libs//bootstrap-toggle/bootstrap-toggle.min.js') !!}
    @endif

    @if(isset($lib['datetimepicker']))
    {!! Html::script('assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    @endif
    {!! Html::script('assets/js/app.min.js') !!}
    {!! Html::script('assets/js/script.js') !!}



    @if (count($errors) > 0)
    @foreach ($errors->all() as $error)

    <script>
        $(document).ready(function() {
            show_message('{{ $error }}');
        });

    </script>
    @endforeach
    @endif



    @if (Session::has('success'))
    <script>
        toastr.success("{{ Session::get('success') }}");

    </script>
    @elseif (Session::has('error'))
    <script>
        toastr.error("{{ Session::get('error') }}");

    </script>
    @endif

    @yield('ext_lib')

    @yield('custom_script')

</body>

</html>