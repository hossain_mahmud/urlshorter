
<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ URL('admin/dashboard') }}" class="waves-effect waves-light">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li class="{{ request()->is('admin/shorter/create') ? 'mm-active' : '' }}">
                    <a href="{{ URL('admin/shorter/create') }}"
                        class="waves-effect waves-light {{ request()->is('admin/shorter/create') ? 'active' : '' }}">
                        <i class="fa fa-mail-bulk"></i>
                        <span> Shorter </span>
                    </a>
                </li>
                
                <li class="{{ request()->is('admin/shorter') ? 'mm-active' : '' }}">
                    <a href="{{ URL('admin/shorter') }}"
                        class="waves-effect waves-light {{ request()->is('admin/shorter') ? 'active' : '' }}">
                        <i class="fa fa-images"></i>
                        <span> Shorter List</span>
                    </a>
                </li>

            </ul>

        </div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->