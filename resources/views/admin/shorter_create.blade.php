@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="card-box col-md-12">
            <h4 class="header-title">Create New Shorter</h4>

            <div class="row">


                @if (isset($edit))
                    {!! Form::model($shorter, ['method' => 'put', 'route' => ['shorter.update', $shorter->id], 'class'
                    => 'form-horizontal', 'role' => 'form']) !!}
                @else
                    {!! Form::open([
                    'route' => 'shorter.store',
                    'class' => 'form_validated form-horizontal form-label-left
                    col-md-12',
                    'role' => 'form',
                    ]) !!}
                @endif

                @php
                if(isset($edit)){
                $parent_id = $shorter->parent_id;
                }else{
                $parent_id = null;
                }
                @endphp



                <div class="form-group col-md-12{{ $errors->has('url') ? ' has-error' : '' }}">
                    {!! Form::label('url', 'URL', ['class' => 'control-label']) !!}
                    <span class="required text-danger">*</span>

                    <div>
                        {!! Form::text('url', null, ['class' => 'form-control placeholder', 'parsley-trigger' => 'change',
                        'required' => 'required']) !!}
                        <span class="help-block text-danger">
                            {!! $errors->first('url') !!}
                        </span>
                    </div>
                </div>

                <div class="col-md-4 form-group">
                    {!! Form::label('domain', 'Alias for Perfix', ['class' => 'control-label']) !!}
                    <div>
                        {!! Form::text('domain', null, ['class' => 'form-control placeholder']) !!}
                    </div>
                </div>

                <div class="col-md-4 form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
                    {!! Form::label('expiry_date', 'Expiry Date', ['class' => 'control-label']) !!}
                    <span class="required text-danger">*</span>

                    <div>
                        {!! Form::text('expiry_date', $expiry_date, ['class' => 'form-control placeholder datetime',
                        'parsley-trigger' =>'change',
                        'required' => 'required','autocomplete'=>'off']) !!}
                        <span class="help-block text-danger">
                            {!! $errors->first('expiry_date') !!}
                        </span>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
                    <div>
                        <input type="hidden" name="type" value="0">
                        <input type="checkbox" class="slug_toggle" data-toggle="toggle" value="0" data-on="Private"
                            data-off="Public">
                    </div>
                </div>

                <div class="form-group">
                    <div class=" pull-right">
                        <button class="btn btn-primary" type="button">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

                </form>

            </div>
        </div>
        
    </div>

@stop
