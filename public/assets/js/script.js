/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var table
var images_flug = ""
var $token = ($token)?$token:'';

$(document).ready(function () {


  if($(".datepicker1").length > 0){
    $(".datepicker1").datepicker({
      autoclose: !0,
      todayHighlight: !0,
      format: "yyyy-mm-dd",
    })
  }

  if($(".datetime").length >0){
    // 11 October 2021 - 01:15 PM

    $(".datetime").datetimepicker({
        fontAwesome:true,
        format: "dd MM yyyy - HH:ii P",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
  }

  if($(".slug_toggle").length >0){
    $(".slug_toggle").bootstrapToggle({
      on: "Enabled",
      off: "Disabled",
    })
    $("body").on('change','.slug_toggle',function(){
        if(this.checked){
          $('[name="type"]').val(1);
        }else{
          $('[name="type"]').val(0);
        }
    });
  }


  $(".datatable_normal").each(function () {
    $(".datatable_normal").DataTable()
  })


  $("#datatable_by_ajax").each(function () {
    var url = urlD + "/" + $(this).attr("method")
    var columns_attr = $(this).attr("columns")
    var columns_split = columns_attr.split(",")
    var columns = []
    $.each(columns_split, function (k, val) {
      columns.push({ data: val })
    })
    table = $("#datatable_by_ajax").DataTable({
      paging: true, // Table pagination
      ordering: true, // Column ordering
      info: true, // Bottom left status text
      width: "98%",
      processing: true,
      serverSide: true,
      aLengthMenu: [
        [10, 50, 100, 500],
        [10, 50, 100, 500],
      ],
      iDisplayLength: 10,
      ajax: {
        url: url,
        type: "POST",
        data: function (d) {},
      },
      columns: columns,

      columnDefs: [
        {
          orderable: false,
          targets: 0,
        },
      ],
    })
  })

  $(".placeholder").each(function () {
    $(this).attr(
      "placeholder",
      "Write " + $(this).parent().parent().children().eq(0).text()
    )
  })

  $("body").delegate(".delete_ajax", "click", function () {
    var $this = $(this)
    var relative = $(this).attr("action")
    var $message = "are you sure you want to delete this item?"

    if (relative.search("tag") != -1 ||
     relative.search("category") != -1 ||
     relative.search("showcard") != -1) {
      $message =
        "This item have related post.<br> are you sure you want to delete all item?"
    }

    bootbox.confirm({
      message: $message,
      buttons: {
        confirm: {
          label: "Yes",
          className: "btn-success",
        },
        cancel: {
          label: "No",
          className: "btn-danger",
        },
      },
      callback: function (result) {
        if (result == true) {
          var dialog = bootbox.dialog({
            message:
              '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Please wait while we do something...</p>',
            closeButton: false,
          })

          var response = custom_ajax($($this).attr("action"), {
            _method: "DELETE",
          })
          show_message(response.msg)
          if (response.status == undefined || response.status == "1") {
            $($this)
              .parent()
              .parent()
              .hide("slow", function () {
                $($this).remove()
                dialog.modal("hide")
                table.draw("page")
              })
          } else {
            bootbox.alert(response.msg)
            setTimeout(() => {
              dialog.modal("hide")
            }, 500)
          }
        }
      },
    })
  })

  $("div.alert").not(".alert-important").delay(3000).fadeOut(350)

  if($(".select2_normal").length > 0){
  $(".select2_normal").select2()
  }

if($(".select2_tags_normal").length > 0){
  $(".select2_tags_normal").select2({
      tags: true,
      placeholder:'Select some items',
      createTag: function(params) {
          return undefined;
      }
  })
}

if($(".select2_tags_normal2").length > 0){
  $(".select2_tags_normal2").select2({
      tags: true,
      placeholder:'Select or new some items',
  })
}


  $("body").delegate(".copyToClipboard", "click", function (e) {
    var txt = $(this).attr("txt")
    copyToClipboard(txt)
    show_message("Copied URL", "i")
    return false;
  })



  //
})

function get_data_select2(selectedObject) {
  var res = ""
  if ($(selectedObject).select2("data") != null) {
    res = $(selectedObject).select2("data")
  }
  return res
}
function get_text_select2(selectedObject) {
  var res = ""
  if ($(selectedObject).select2("data") != null) {
    res = $(selectedObject).select2("data").text
  }
  return res
}
function get_id_select2(selectedObject) {
  var res = ""

  if ($(selectedObject).select2("data") != null) {
    res = $(selectedObject).select2("data")[0].id
  }
  return res
}

function copyToClipboard(text) {
  var $temp = $("<input>")
  $("body").append($temp)
  $temp.val(text).select()
  document.execCommand("copy")
  $temp.remove()
}

function log($str) {
  console.log($str)
}

function show_message($msg, $type) {
  toastr.options = {
    closeButton: true,
    debug: false,
    newestOnTop: true,
    progressBar: true,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    showDuration: "300",
    hideDuration: "1000",
    timeOut: "5000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut",
  }

  switch ($type) {
    case "sa":
      toastr.options["timeOut"] = "30000"
      toastr.success($msg)
      break

    case "i":
      toastr.info($msg)
      break

    case "w":
      toastr.warning($msg)
      break

    case "s":
      toastr.success($msg)
      break

    case "e":
      toastr.error($msg)
      break

    default:
      toastr.error($msg)
  }
}

$(function ($) {
  var obj = {}
  obj["_token"] = $token
  $.ajaxSetup({
    data: obj,
    dataType: "jsonp",
  })
})

function custom_ajax(url, data, async, calback) {
  //    data['_token'] = $token;
  if (async == undefined) {
    async = false
  }

  var response = ""
  $.ajax({
    url: urlD + "/" + url,
    crossDomain: true,
    dataType: "json",
    data: data,
    type: "post",
    async: async,
    success: function (data) {
      if (calback != undefined) {
        calback(data)
      } else {
        response = data
      }
    },
  })

  if (calback == undefined) {
    return response
  }
}
function custom_ajax_other(url, data, async, calback) {
  //    data[_token_name] = _token_hash;
  if (async == undefined) {
    async = false
  }

  var response = ""
  $.ajax({
    url: url,
    crossDomain: true,
    dataType: "JSONP",
    data: data,
    type: "POST",
    async: async,
    success: function (data) {
      if (calback != undefined) {
        calback(data)
      } else {
        response = data
      }
    },
  })

  if (calback == undefined) {
    return response
  }
}

function get_thumbnails_rw($src = "", $size = "") {
  var str_split = $src.split("uploaded_files")
  if (str_split.length == 2) {
    var img_name = str_split[1].split("_")
    var img_name_f = ""

    for (var $i = 1; $i <= img_name.length; $i++) {
      if ($i == img_name.length) {
        img_name_f += "_" + $size + "_"
      }
      img_name_f += img_name[$i - 1]
    }

    return str_split[0] + "uploaded_files/thumbnail" + img_name_f
  }
}
function get_thumbnails($src = "", $size = "") {
  if ($src == "") {
    return ""
  } else {
    switch ($size) {
      case "s":
        $size = "small"
        break
      case "m":
        $size = "medium"
        break
      case "l":
        $size = "large"
        break

      default:
        $size = "all"
        break
    }

    if ($size == "all") {
      return {
        small: get_thumbnails_rw($src, "small"),
        medium: get_thumbnails_rw($src, "medium"),
        large: get_thumbnails_rw($src, "large"),
      }
    } else {
      return get_thumbnails_rw($src, $size)
    }
  }
}

var obj = {}
obj["_token"] = $token
$.ajaxSetup({
  data: obj,
  dataType: "jsonp",
})
