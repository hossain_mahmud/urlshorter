
#  URL Shortener Application

URL shorteners spit out links that are easier to share.


## Getting Started

This project by Laravel 8
using passport api authentication.

## The project will have following features-

● URL can be shorten and registered user can give a unique alias of it.

● This link is short enough to be easily copied and pasted
into applications.

● When users access a short link, our service should redirect
them to the original link.

● Users should optionally be able to pick a custom short link for
their URL.

● Links will expire after a standard default time-span. Users
should be able to specify the expiration time.

● Shortened links should not be predictable.

● Full-fledged registration and login system.

● Proper validation for each form.

● Option for private URL.

● Basic statistics.

● Basic Admin Panel.


Application can be used both in logged in or guest users. But for
using the full potential features, you should be logged in.

## Admin User
There is no way to create an admin user from the UI. You need to
change the value of is_admin in the users table directly from the
database to gain admin access.

## API
1. Login
2. User-Details
3. Logout
4. Shorter your link (CRUD).

## API Postman Collection file on your project root folder name of
`URL_shorter.postman_collection.json`



### How To Install

1. git clone
2. cd this folder
3. command run by terminal
    ```composer update```
4. create database and this database name include .env file
5. command run by terminal
    ```php artisan migrate```
6. command run by terminal
    ```php artisan serve```

##   Done


## Demo Link



http://192.53.118.247/urlshorter/

email = mithunraj015@gmail.com

password = mithunraj015



