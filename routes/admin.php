<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'DashboardController@index');
Route::get('dashboard', 'DashboardController@index');

// Shorter

Route::resource('shorter', 'ShorterController');
Route::post('shorterAjax', 'ShorterController@ajax');

// Profile

Route::get('profile', 'ProfileController@index');
Route::post('profile/update', 'ProfileController@update');