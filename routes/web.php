<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->middleware('admin')
    ->namespace('Admin')->group(base_path('routes/admin.php'));

Route::post('url_shorter', [App\Http\Controllers\HomeController::class, 'url_shorter'])->name('url_shorter');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

// Route::get('urlRedirect', function (Request $request) {
//     $explode = explode($request->url, '-');
// });

Auth::routes();

Route::get('/urlRedirect', [App\Http\Controllers\HomeController::class, 'urlRedirect'])->name('urlRedirect');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('logout', 'Auth\LoginController@logout');
