<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShortersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shorters', function (Blueprint $table) {
            $table->id();
            $table->text('url');
            $table->string('short');
            $table->string('domain')->nullable();
            $table->dateTime('expired');
            // 0 =
            $table->integer('type')->default('0')
                ->comment('0 = public, 1 = private');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shorters');
    }
}
