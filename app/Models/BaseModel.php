<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Uuid;

class BaseModel extends Eloquent
{


    public function scopeUuidToID($query, $uuid)
    {
        return $query->select('id')->where('uuid', $uuid)->first();
    }

    public function scopeGetLastSlug($query, $slugName)
    {
        $data = $query->where('slug', 'like', $slugName . '%')->count();
        if (!empty($data)) {
            return $slugName . '-' . ($data + 1);
        }
        return $slugName;
    }


    public function scopeList($query, $key1 = 'name', $key2 = 'id')
    {
        return $query->pluck($key1, $key2)->all();
    }


    public function scopeGetName2id($query, $name)
    {
        return $query->where('name', $name)->first();
    }
    public function scopeGetId2Name($query, $name)
    {
        return $query->where('id', $name)->first()->name;
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            // dd($model->column);
            $columns = \Schema::getColumnListing($model->table);
            if (in_array('uuid', $columns)) {
                $model->uuid = (string) Uuid::generate();
            }
            if (in_array('created_by', $columns) && Auth::check()) {
                $user = Auth::user();
                $model->created_by = $user->id;
            }
            if (in_array('updated_by', $columns) && Auth::check()) {
                $user = Auth::user();
                $model->updated_by = $user->id;
            }
            // if ($model->table != 'view_page' && $model->table != 'view_page_log') {

            // }
        });
        static::updating(function ($model) {
            $columns = \Schema::getColumnListing($model->table);
            if (in_array('updated_by', $columns)) {
                // if ($model->table == 'view_page' && $model->table == 'view_page_log') {
                $user = Auth::user();
                $model->updated_by = $user->id;
            }
        });
    }
}
