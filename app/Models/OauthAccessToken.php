<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel as Model;

class OauthAccessToken extends Model
{
    use HasFactory;
}
