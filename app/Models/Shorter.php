<?php

namespace App\Models;

use App\Models\BaseModel as Model;

class Shorter extends Model
{
    protected $table = 'shorters';

    protected $fillable = ['url', 'short', 'type', 'domain', 'expired'];
    //


    public function post()
    {
        return $this->hasMany(Post::class);
    }
}
