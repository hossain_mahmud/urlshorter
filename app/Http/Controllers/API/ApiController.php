<?php


namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class ApiController extends Controller
{

    function response($data = [], $first = '')
    {
        if (!isset($data[0]) && empty($data[0]) && $first == '') {
            return $this->sendError('no data found.');
        } else if (empty($data) && $first == 'first') {
            return $this->sendError('no data found.');
        } else {
            return $this->sendResponse($data, 'data found.');
        }
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message = '')
    {
        $response = [
            'success' => true,
            'dataLength'    => '',
            'message' => $message,
            'data'    => $result,
        ];

        if (isset($result[0])) {
            $response['dataLength'] = count($result);
        }


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        $response['data'] = $errorMessages;

        return response()->json($response, $code);
    }
}
