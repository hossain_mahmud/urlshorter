<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController as ApiController;
use App\Http\Requests\ShorterRequestApi;

use App\Models\Shorter;
use Auth;

class ShorterController extends ApiController
{



    public function store(ShorterRequestApi $request)
    {

        $short =  __uniqueString();

        $insert = Shorter::create([
            'url'     => $request->url,
            'domain'  => (empty($request->domain)) ? '' : $request->domain,
            'short'   => $short,
            'type'    => $request->type,
            'expired' => $request->expiry_date,
        ]);
        $data = Shorter::find($insert->id);
        $data->redirect = URL('urlRedirect?url=' . $data->domain . '-' . $data->short);

        return $this->sendResponse($data, 'New Shorter Created.');
    }

    public function update(ShorterRequestApi $request, $id)
    {
        $check = Shorter::where(['id' => $id, 'created_by' => Auth::id()])->first();

        if (!empty($check)) {
            Shorter::where('id', $id)->update([
                'url'     => $request->url,
                'domain'  => (empty($request->domain)) ? '' : $request->domain,
                'type'    => $request->type,
                'expired' => $request->expiry_date,
            ]);

            $data = Shorter::find($id);
            $data->redirect = URL('urlRedirect?url=' . $data->domain . '-' . $data->short);

            return $this->sendResponse($data, 'Shorter Updated.');
        } else {
            return $this->sendError('ID Not Found.');
        }
    }

    public function destroy($id)
    {
        $check = Shorter::where(['id' => $id, 'created_by' => Auth::id()])->first();
        if (!empty($check)) {
            Shorter::where('id', $id)->delete();
            return $this->sendResponse([], 'Shorter Deleted.');
        } else {
            return $this->sendError('Shorter Not Found.');
        }
    }

    public function index()
    {
        $data = Shorter::where('created_by', Auth::id())->paginate(10);
        return $this->sendResponse($data, 'Shorter Data');
    }
}
