<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController as ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
use DB;

class UserController extends ApiController
{
    public $successStatus = 200;

    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();
            return $this->sendResponse([], 'User logout successfully.');
        } else {
            return $this->sendError('please login first');
        }
    }

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'type' => 'is_admin'])) {
            $data = Auth::user();
            $data->token = Auth::user()->createToken('url-shorter')->accessToken;

            return $this->sendResponse($data, 'User login successfully.');
        } else {
            return $this->sendError('your email and password not match.');
        }
    }
}
