<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shorter;
use DB;
use Carbon\Carbon;
use Flash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //  public function

    public function url_shorter(Request $request)
    {

        $expiry_date = Carbon::now()->addDays(7)->format('Y-m-d');
        $short       = __uniqueString();

        $data = Shorter::create([
            'url'     => $request->url,
            'domain'  => '',
            'short'   => $short,
            'type'    => '0',
            'expired' => $expiry_date
        ]);
        Flash::success('URL Shorter Created.');

        return redirect()->route('home')->with(compact('data'));
    }

    public function urlRedirect(Request $request)
    {
        $explode = explode('-', $request->url);
        $result  = Shorter::where(['short' => $explode[1], 'domain' => $explode[0]])->first();

        if (empty($result)) {
            dd($explode);
            echo  "No result found";
        }
        if (date('Y-m-d H:i:s') > $result->expired) {
            dd("Already expired");
        }
        return redirect($result->url);
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $recentList = Shorter::orderBy('id', 'desc')->take(5)->get();
        return view('dashboard')->with(compact('recentList'));
    }
}
