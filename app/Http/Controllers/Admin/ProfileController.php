<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use Flash;
use Response;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $company_list;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.profile');
    }

    public function update(ProfileRequest $request, User $user)
    {

        $result = $user->where(['id' => Auth::id()])->first();
        $msg_type = 'error';
        if (!empty($result)) {

            if (Hash::check($request->old_password, $result->password)) {

                $user->where('id', Auth::id())->update([
                    'name' => $request->name,
                    'password' => Hash::make($request->new_password),
                    'type' => '2',
                ]);
                $msg = "Your Profile Updated.";
                $msg_type = 'success';
            } else {
                $msg = "Password Not Match";
            }
        } else {
            $msg = "User Not found";
        }


        Flash::$msg_type($msg);

        return redirect("admin/profile");
    }
}
