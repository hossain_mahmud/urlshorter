<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shorter;
use App\Http\Requests\ShorterRequest;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Flash;
use Response;
use Carbon\Carbon;



class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $result = Shorter::all();
        $result = Shorter::select(
            DB::raw('count(id) as count'),
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as date")
          )
          ->groupBy('date')->orderBy('date')->get();
        //   dd($result);

        return view('admin.dashboard')
            ->with(compact('result'));
    }



    public function ajax(Request $request)
    {

        if ($request->ajax()) {

            $collection_data = Shorter::orderBy('id', 'desc')
                ->get();


            return Datatables::collection($collection_data)
                ->addColumn('action', function ($row) {
                    return '<a href="#" class="btn btn-success waves-effect btn-sm waves-light copyToClipboard"
                    txt="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '" role="button">
                    <i class="fa fa-copy"></i> Copy</a>
                    <a class="btn btn-info btn-sm" href="' . URL('admin/shorter/' . $row->id . '/edit') . '"> <i class="fa fa-edit"></i> Edit</a>
                    <a class="btn btn-danger btn-sm delete_ajax" href="javascript:void(0)" action="' . 'admin/shorter/' . $row->id . '"> <i class="fa fa-trash"></i> Delete</a>';
                })
                ->addColumn('original_url', function ($row) {
                    return '<a class="btn btn-info btn-sm" target="url" href="' . $row->url . '"> Open Link</a>';
                })
                ->addColumn('short_url', function ($row) {
                    return '<a class="btn btn-info btn-sm" target="url" href="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '"> Open Link</a>';
                })
                ->editColumn('type', function ($row) {
                    return ($row->type == '0' ? 'Public' : 'Private');
                })
                ->rawColumns(['url' => true])
                ->make(true);
        }
    }

}
