<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shorter;
use App\Http\Requests\ShorterRequest;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Flash;
use Response;
use Carbon\Carbon;
use Auth;



class ShorterController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $expiry_date = Carbon::now()->addDays(7)->format('Y-m-d');

        $lib = [
            'datatable' => '',
        ];
        return view('admin.shorter_index')
            ->with(compact('lib', 'expiry_date'));
    }


    public function create()
    {

        // $expiry_dateF = Carbon::createFromFormat('d F Y -  g:i A',$request->expiry_date);
        $expiry_date = Carbon::now()->addDays(7)->format('d F Y - h:i A');
        // dd($time);

        $lib = [
            'datetimepicker' => '',
            'bootstrap-toggle' => '',
        ];
        return view('admin.shorter_create')
            ->with(compact('lib', 'expiry_date'));
    }



    public function ajax(Request $request)
    {

        if ($request->ajax()) {

            $collection_data = Shorter::where('created_by', Auth::id())
                ->orderBy('id', 'desc')
                ->get();


            return Datatables::collection($collection_data)
                ->addColumn('action', function ($row) {
                    return '<a href="#" class="btn btn-success waves-effect btn-sm waves-light copyToClipboard" title="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '"
                    txt="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '" role="button">
                    <i class="fa fa-copy"></i> Copy</a>
                    <a class="btn btn-info btn-sm" href="' . URL('admin/shorter/' . $row->id . '/edit') . '"> <i class="fa fa-edit"></i> Edit</a>
                    <a class="btn btn-danger btn-sm delete_ajax" href="javascript:void(0)" action="' . 'admin/shorter/' . $row->id . '"> <i class="fa fa-trash"></i> Delete</a>';
                })
                ->addColumn('original_url', function ($row) {
                    return '<a class="btn btn-info btn-sm" target="url" href="' . $row->url . '" title="' . $row->url . '"> Open Original Link</a>';
                })
                ->addColumn('short_url', function ($row) {
                    return '<a class="btn btn-info btn-sm" target="url" title="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '" href="' . URL('urlRedirect?url=' . $row->domain . '-' . $row->short) . '"> Open Short Link</a>';
                })
                ->editColumn('expired', function ($row) {
                    $expiry_dateF = Carbon::createFromFormat('Y-m-d H:i:s', $row->expired);
                    return $expiry_dateF->format('d F Y - h:i A');
                })

                ->editColumn('type', function ($row) {
                    return ($row->type == '0' ? 'Public' : 'Private');
                })
                ->rawColumns(['url' => true])
                ->make(true);
        }
    }

    public function store(ShorterRequest $request)
    {

        $short =  __uniqueString();

        $expiry_dateF = Carbon::createFromFormat('d F Y - h:i A', $request->expiry_date);
        $expiry_date = $expiry_dateF->format('Y-m-d H:i:s');

        Shorter::create([
            'url'     => $request->url,
            'domain'  => (empty($request->domain)) ? '' : $request->domain,
            'short'   => $short,
            'type'    => $request->type,
            'expired' => $expiry_date,
        ]);
        Flash::success('URL Shorter Created.');

        return redirect("admin/shorter/create");
    }


    public function edit(Shorter $shorter)
    {
        $lib = [
            'datetimepicker' => '',
            'bootstrap-toggle' => '',
        ];

        $expiry_dateF = Carbon::createFromFormat('Y-m-d H:i:s', $shorter->expired);
        $expiry_date = $expiry_dateF->format('d F Y - h:i A');

        $edit = 'true';
        return view('admin.shorter_create')->with(compact('shorter', 'expiry_date', 'lib', 'edit'));
    }

    public function update(ShorterRequest $request, Shorter $shorter)
    {

        $expiry_dateF = Carbon::createFromFormat('d F Y - h:i A', $request->expiry_date);
        $expiry_date = $expiry_dateF->format('Y-m-d H:i:s');

        $update_data = $request->all();

        $shorter->update($update_data);
        Flash::success('URL Shorter Updated.');

        return redirect("admin/shorter");
    }


    public function destroy(Shorter $shorter)
    {
        $shorter->delete();
        return Response::json(['msg' => "Shorter Deleted."]);
    }
}
